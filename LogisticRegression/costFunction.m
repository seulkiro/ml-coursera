%% Compute the cost using theta as the parameter for logistic regression
%  and the gradient of the cost w.r.t. to the parameters
function [J, grad] = costFunction(theta, X, y)
    % number of training examples
    m = length(y);
    % compute h(x)
    h = sigmoid(theta' * X')';
    % compute cost
    J = (1/m)*sum((-y.*log(h))-(1-y).*log(1-h));
    % compute gradient of cost wrt theta
    grad = (1/m)*((h-y)'*X);
end

