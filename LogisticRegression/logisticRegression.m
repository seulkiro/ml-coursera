%% Logistic Regression
%
%  Logistic Regression to learn a function f(x) given a set of training
%  examples (ex2data1.txt) that contain the students' scores of exam 1 and 2
%  as well as whether the students are admitted for the next class.
%  h(x) is a good classifier to predict
%
%  Note: The following logistic regression was implemented as part of
%  the Andrew Ng's Coursera ML course. The data set and some of the code below
%  is from the Coursera ML course.
%
%  https://www.coursera.org/learn/machine-learning/
%
function logisticRegression
    % initialization
    clear ; close all;

    % add shared function path for sigmoid function
    addpath('../SharedFunctions');

    % load Data
    % - data(:, 1): exam 1 score
    % - data(:, 2): exam 2 score
    % - data(:, 3): label, whether student is admitted to next class
    data = load('ex2data1.txt');
    % feature matrix
    X = data(:, [1, 2]);
    % label vector
    y = data(:, 3);

    % plot the data
    figure 1;
    hold on;
    % find indices of positive and negative examples
    pos = find(y == 1);
    neg = find(y == 0);
    % plot examples
    plot(X(pos, 1), X(pos, 2), 'k+', 'LineWidth', 2, 'MarkerSize', 4);
    plot(X(neg, 1), X(neg, 2), 'ko', 'MarkerFaceColor', 'y', 'MarkerSize', 4);
    % labels and legend
    xlabel('Exam 1 score')
    ylabel('Exam 2 score')
    % Specified in plot order
    legend('Admitted', 'Not admitted')
    hold off;

    % use a built-in function (fminunc) to find the optimal parameters theta
    % options for fminunc
    options = optimset('GradObj', 'on', 'MaxIter', 400);
    % run fminunc to obtain the optimal theta
    [m, n] = size(X)
    X = [ones(m, 1) X];
    initial_theta = zeros(n + 1, 1);
    [theta, cost] = ...
        fminunc(@(t)(costFunction(t, X, y)), initial_theta, options);
    fprintf('Cost at theta found by fminunc: %f\n', cost);
    fprintf('Theta: %f\n', theta);

    % plot the decision boundary
    % only need 2 points to define a line, so choose two endpoints
    hold on;
    dbx = [min(X(:,2)),  max(X(:,2))];
    % calculate the decision boundary line
    dby = (-1./theta(3)).*(theta(2).*dbx + theta(1));
    % plot and adjust axes for better viewing
    plot(dbx, dby)
    % legend, specific for the exercise
    legend('Admitted', 'Not admitted', 'Decision Boundary')
    axis([30, 100, 30, 100])
    hold off;

    % predict probability for a student with score 45 on exam 1 
    % and score 85 on exam 2 
    prob = sigmoid([1 45 85] * theta);
    fprintf(['For a student with scores 45 and 85, we predict an admission ' ...
        'probability of %f\n'], prob);

    % compute the training and test set accuracies of the model
    p = sigmoid(theta'*X')' >= 0.5;
    fprintf('Train Accuracy: %f\n', mean(double(p == y)) * 100);
end
