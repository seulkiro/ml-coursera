% Multivariate Linear Regression to learn a function h(x) given a set of
% housing prices (y) with the size of the houses (x1) and number of bedrooms
% (x2) so that h(x) is a good predictor for the target price
%
% Note: The following multivariate linear regression was implemented as part of
% the Andrew Ng's Coursera ML course. The data set and some of the code below
% is from the Coursera ML course.
%
% https://www.coursera.org/learn/machine-learning/
%
function multivariateLinearRegressionWithGradientDescent
    % add gradient descent path
    addpath('../GradientDescent');

    % load data from ex1data2
    % - data(:, 1): size of house
    % - data(:, 2): number of bedromms
    % - data(:, 3): price
    data = load('ex1data2.txt');
    % feature matrix (size of house and number of bedrooms)
    X = data(:, 1:2);
    % label vector (price)
    y = data(:, 3);
    % # of training data
    m = length(y);

    % print out some data points
    fprintf('First 10 examples from the dataset: \n');
    fprintf(' x = [%.0f %.0f], y = %.0f \n', [X(1:10,:) y(1:10,:)]');
    fprintf('\n');

    % feature scaling
    x1 = X(:, 1);
    x2 = X(:, 2);
    mu = [ mean(x1) mean(x2) ];
    sigma = [ std(x1) std(x2) ];
    normalizedX = X - repmat(mu, rows(x1), 1);
    normalizedX = normalizedX ./ repmat(sigma, rows(x1), 1);

    % add ones for x0
    normalizedX = [ ones(m, 1) normalizedX ];

    % run gradient descent
    alpha = 0.13;
    iterations = 50;
    ncols = size(normalizedX, 2);
    theta = zeros(ncols, 1);
    [theta, J_history] = gradientDescent(normalizedX, y, theta, alpha, iterations);

    % display gradient descent's result
    fprintf('Theta computed from gradient descent: \n');
    fprintf(' %f \n', theta);
    fprintf('\n');

    % run gradient descent with different learning rates for comparison
    [theta1, J_history1] = gradientDescent(normalizedX, y, zeros(ncols, 1), 0.03, 50);
    [theta2, J_history2] = gradientDescent(normalizedX, y, zeros(ncols, 1), 0.06, 50);
    [theta3, J_history3] = gradientDescent(normalizedX, y, zeros(ncols, 1), 0.09, 50);

    % plot the convergence graph
    figure 1;
    plot(1:numel(J_history), J_history, '-b', 'LineWidth', 2);
    hold on;
    plot(1:numel(J_history1), J_history1, '-r', 'LineWidth', 2);
    plot(1:numel(J_history2), J_history2, '-k', 'LineWidth', 2);
    plot(1:numel(J_history3), J_history3, '-g', 'LineWidth', 2);
    xlabel('Number of iterations');
    ylabel('Cost J');

    % estimate the price of a 1650 sq-ft, 3 br house
    sample = [ 1650 3 ];
    % normalize it before prediction
    normalizedSample = (sample - mu) ./ sigma;
    normalizedSample = [ 1 normalizedSample ];
    % predict the price
    price = theta' * normalizedSample';

    fprintf(['Predicted price of a 1650 sq-ft, 3 br house ' ...
             '(using gradient descent):\n $%.2f\n'], price);
end
