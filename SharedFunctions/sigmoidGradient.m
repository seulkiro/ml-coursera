%% Compute gradient of sigmoid
%
% g' = sigmoid(z) .* (1- sigmoid(z)) computes the gradient of sigmoid.
% z can be a matrix, vector, or scalar.
function g = sigmoidGradient(z)
    g = sigmoid(z) .* (1 - sigmoid(z));
end
