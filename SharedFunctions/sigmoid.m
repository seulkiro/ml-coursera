%% Compute sigmoid function
%
%  g = sigmoid(z) computes the sigmoid of z.
%  z can be a matrix, vector, or scalar.
function g = sigmoid(z)
    g = 1 ./ (1 + exp(-z));
end
