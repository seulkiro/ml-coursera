%% Multi-class Classification using Neural Networks with Backpropagation
%
%  This develops a model to predict a hand writtern digit depicted in gray
%  20x20 by training a subset of MNIST dataset stored in Matlab binary format.
%
%  Note: The following neural networks' backpropagation was implemented as part of
%  the Andrew Ng's Coursera ML course. The data set and some of the code below
%  is from the Coursera ML course.
%
%  https://www.coursera.org/learn/machine-learning/
%
function neuralNetworks
    % initialization
    clear; close all;

    % add shared function path for sigmoid function
    addpath('../SharedFunctions');
    % add multi-class classification to re-use MNIST data set as well as
    % useful functions; fmincg, displayData, etc
    addpath('../MultiClassClassification');

    % setup the parameters
    input_layer_size = 400; % 20x20 input images of digits
    hidden_layer_size = 25; % 25 hidden units
    num_labels = 10;        % 10 labels, from 1 to 10 mapping 0 to 9

    % load training data set
    fprintf('Loading and Visualizing Data ...\n')
    load('../MultiClassClassification/ex3data1.mat');
    [m n] = size(X);

    % randomly select 100 data points to display
    rand_indices = randperm(m);
    sel = X(rand_indices(1:100), :);
    displayData(sel);

    % load the pre-calculated weights into variables Theta1 and Theta2
    fprintf('Loading Saved Neural Network Parameters ...\n\n')
    load('../MultiClassClassification/ex3weights.mat');

    % unroll parameters 
    nn_params = [Theta1(:) ; Theta2(:)];

    fprintf('Initializing Neural Network Parameters ...\n\n')

    initial_Theta1 = randInitializeWeights(input_layer_size, hidden_layer_size);
    initial_Theta2 = randInitializeWeights(hidden_layer_size, num_labels);
    % unroll parameters
    initial_nn_params = [initial_Theta1(:) ; initial_Theta2(:)];

    fprintf('Training Neural Network... \n\n')

    options = optimset('MaxIter', 50);
    lambda = 1;

    % create "short hand" for the cost function to be minimized
    costFunction = @(p) nnCostFunction(p, ...
                                       input_layer_size, ...
                                       hidden_layer_size, ...
                                       num_labels, X, y, lambda);

    % costFunction is a function that takes in only one argument (the
    % unrolled neural network parameters)
    [nn_params, cost] = fmincg(costFunction, initial_nn_params, options);

    % obtain Theta1 and Theta2 back from nn_params
    Theta1 = reshape(nn_params(1:hidden_layer_size * (input_layer_size + 1)), ...
                     hidden_layer_size, (input_layer_size + 1));

    Theta2 = reshape(nn_params((1 + (hidden_layer_size * (input_layer_size + 1))):end), ...
                     num_labels, (hidden_layer_size + 1));

    fprintf('Visualizing Neural Network by displaying hidden units... \n\n')
    displayData(Theta1(:, 2:end));

    % training set accuracy
    pred = predict(Theta1, Theta2, X);
    fprintf('Training Set Accuracy: %f\n', mean(double(pred == y)) * 100);

end
