%% Randomly initialize weights
%
% epsilon is computed using
% e = sqrt(6) / (sqrt(#ofInputLayer) + sqrt(#ofOutputLayer + 1))
function W = randInitializeWeights(L_in, L_out)
    % Randomly initialize the weights to small values
    epsilon_init = 0.12082;
    W = rand(L_out, 1 + L_in) * 2 * epsilon_init - epsilon_init;
end
