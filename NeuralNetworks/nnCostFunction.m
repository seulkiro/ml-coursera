% Compute the cost with feedforward and gradient with backpropagation.
% Note that the theta parameters are unrolled in nn_params.
function [J grad] = nnCostFunction(nn_params, ...
                                   input_layer_size, ...
                                   hidden_layer_size, ...
                                   num_labels, ...
                                   X, y, lambda)
    % reshape nn_params back into the parameters Theta1 and Theta2, the weight matrices
    % for our 2 layer neural network
    Theta1 = reshape(nn_params(1:hidden_layer_size * (input_layer_size + 1)), ...
                     hidden_layer_size, (input_layer_size + 1));

    Theta2 = reshape(nn_params((1 + (hidden_layer_size * (input_layer_size + 1))):end), ...
                     num_labels, (hidden_layer_size + 1));

    % initialize J and grad variables
    J = 0;
    Theta1_grad = zeros(size(Theta1));
    Theta2_grad = zeros(size(Theta2));

    % setup some useful variables
    m = size(X, 1);
             
    % iterate through the training examples and labeled data
    cost = 0;
    for i = 1:m
        % y vector as a 10-dimensional vector with only the labeled digit's
        % position marked as 1.
        % e.g. [ 0 0 0 0 0 1 0 0 0 0 ] for 5
        y_vec = zeros(1, num_labels);
        y_vec(:, y(i, :)) = 1;
        % add bias unit to input
        x_vec = [ 1, X(i, :) ];
        % activation function of hidden layer
        z2 = Theta1 * x_vec';
        a2 = sigmoid(z2)';
        % add bias unit to hidden layer
        a2 = [ 1, a2 ];
        % activation function of output layer, which is h(x)
        h = sigmoid(Theta2 * a2')';
        % compute the cost
        cost += (-y_vec * log(h') - (1 - y_vec) * log(1 - h'));

        % compute delta for output layer
        d3 = (h - y_vec)';
        % compute delta for hidden layer
        d2 = Theta2' * d3 .* [ 1; sigmoidGradient(z2) ];
        % remove delta2(1)
        d2 = d2(2:end);

        % accumulate the gradients
        Theta1_grad += (d2 * x_vec);
        Theta2_grad += (d3 * a2);
    end

    % divide the cost by m
    J = cost / m;
    % add cost for the regularization terms
    J += (lambda / (2 * m)) * (sum(sumsq(Theta1(:, 2:end))) + sum(sumsq(Theta2(:, 2:end))));

    % add regularized terms
    Theta1_no_bias = Theta1;
    Theta1_no_bias(:, 1) = 0;
    Theta1_grad = (1 / m) * Theta1_grad + (lambda / m) * Theta1_no_bias;

    Theta2_no_bias = Theta2;
    Theta2_no_bias(:, 1) = 0;
    Theta2_grad = (1 / m) * Theta2_grad + (lambda / m) * Theta2_no_bias;

    % unroll gradients
    grad = [Theta1_grad(:) ; Theta2_grad(:)];
end
