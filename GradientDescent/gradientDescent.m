% Performs gradient descent to learn theta that minimizes the cost for linear
% regression
function [theta, J_history] = gradientDescent(X, y, theta, alpha, iterations)
    % # of training data
    m = length(y);
    % initialize cost vector to visualize it later
    J_history = zeros(iterations, 1);
    % repeat until convergence
    for i = 1:iterations
        % intermediate vector inside the summation of derivative_term
        % i.e. theta * X - y
        term = (theta'*X')'-y;
        % replicate the columns to avoid automatic broadcasting warnings
        term = repmat(term, 1, columns(X));
        % Compute theta using vertorization
        theta = theta - alpha * (1/m) * sum(term .* X)';
        % store the cost J in every iteration    
        J_history(i) = computeCost(X, y, theta);
    end
end
