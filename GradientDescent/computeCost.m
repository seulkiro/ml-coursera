% Compute cost for using theta as the parameter for linear regression to fit
% the data points in X and y
function cost = computeCost(X, y, theta)
    % # of training data
    m = length(y);
    % compute cost
    % - theta: 2x1 dimensional vector
    % - X    : mx2 dimensional matrix, 1st column being all 1's for x0
    % - y    : mx1 dimensional vector
    cost = (1/(2*m)) * sum(((theta' * X')' - y) .^ 2);
end
