%% Regularized Logistic Regression
%
%  Regularized Logistic Regression to predict whether microchips pass quality
%  assurance. ex2data2.txt contains test results on past microchips, from which
%  a logistic regression model can be built.
%
%  Note: The following logistic regression was implemented as part of
%  the Andrew Ng's Coursera ML course. The data set and some of the code below
%  is from the Coursera ML course.
%
%  https://www.coursera.org/learn/machine-learning/
%
function regularizedLogisticRegression
    % initialization
    clear; close all;

    % add shared function path for sigmoid function
    addpath('../SharedFunctions');

    % load Data
    % - data(:, 1): microchip test 1
    % - data(:, 2): microchip test 2
    % - data(:, 3): label, whether microchip passes quality assurance
    data = load('ex2data2.txt');
    % feature matrix
    X = data(:, [1, 2]);
    % label vector
    y = data(:, 3);

    % plot the data
    figure 1;
    hold on;
    % find indices of positive and negative examples
    pos = find(y == 1);
    neg = find(y == 0);
    % plot examples
    plot(X(pos, 1), X(pos, 2), 'k+', 'LineWidth', 2, 'MarkerSize', 4);
    plot(X(neg, 1), X(neg, 2), 'ko', 'MarkerFaceColor', 'y', 'MarkerSize', 4);
    % labels and legend
    xlabel('Microchip Test 1');
    ylabel('Microchip Test 2');
    % specified in plot order
    legend('Passes', 'Not passes');
    hold off;

    % add Polynomial Features
    % this allows us to use logistic regression with data points that are not
    % linearly separable
    X = mapFeature(X(:,1), X(:,2));

    % set regularization parameter lambda to 1
    lambda = 1;

    % use a built-in function (fminunc) to find the optimal parameters theta
    % options for fminunc
    options = optimset('GradObj', 'on', 'MaxIter', 400);
    % run fminunc to obtain the optimal theta
    initial_theta = zeros(size(X, 2), 1);
    [theta, cost] = ...
        fminunc(@(t)(costFunction(t, X, y, lambda)), initial_theta, options);
    fprintf('Cost at theta found by fminunc: %f\n', cost);
    fprintf('Theta: %f\n', theta);

    % plot the decision boundary
    % only need 2 points to define a line, so choose two endpoints
    hold on;
    % Here is the grid range
    u = linspace(-1, 1.5, 50);
    v = linspace(-1, 1.5, 50);

    z = zeros(length(u), length(v));
    % Evaluate z = theta*x over the grid
    for i = 1:length(u)
        for j = 1:length(v)
            z(i,j) = mapFeature(u(i), v(j))*theta;
        end
    end
    z = z'; % important to transpose z before calling contour

    % plot z = 0
    % notice you need to specify the range [0, 0]
    contour(u, v, z, [0, 0], 'LineWidth', 2)
    title(sprintf('lambda = %g', lambda))
    hold off;

    % compute accuracy on our training set
    p = sigmoid(theta'*X')' >= 0.5;

    fprintf('Train Accuracy: %f\n', mean(double(p == y)) * 100);
    fprintf('Expected accuracy (with lambda = 1): 83.1 (approx)\n');
end
