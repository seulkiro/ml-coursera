% Univariate Linear Regression to learn a function h(x) given a set of training
% examples so that h(x) is a good predictor for target y
%
% Note: The following univariate linear regression was implemented as part of
% the Andrew Ng's Coursera ML course. The data set and some of the code below
% is from the Coursera ML course.
%
% https://www.coursera.org/learn/machine-learning/
%
function univariateLinearRegressionWithGradientDescent
    % add gradient descent path
    addpath('../GradientDescent');

    % load data from ex1data1 where it contains population of city and profits
    % of food truck in that city.
    data = load('ex1data1.txt');
    % # of training data
    m = size(data, 1);
    % create feature matrix
    x = data(:, 1);
    X = [ ones(m, 1), x ];
    % create label vector
    y = data(:, 2);

    % plot the data
    figure 1;
    plot(x, y, 'rx', 'MarkerSize', 5);
    xlabel('Population of City in 10,0000s');
    ylabel('Profit in $10,000s');

    % learning rate and # of iterations for gradient descent
    alpha = 0.01;
    iterations = 1500;
    % perform gradient descent
    theta = zeros(2, 1);
    [theta, J_history] = gradientDescent(X, y, theta, alpha, iterations);

    % keep the previous plot visible and draw a straight line to fit the data
    hold on;
    plot(X(:,2), X*theta, '-');
    hold off;

    % visualize cost function J(theta)
    theta0 = linspace(-10, 10, 100);
    theta1 = linspace(-1, 4, 100);
    jvals = zeros(length(theta0), length(theta1));
    for i = 1:length(theta0)
        for j = 1:length(theta1)
            t = [ theta0(i); theta1(j) ];
            jvals(i, j) = computeCost(X, y, t);
        end
    end
    % because of the way meshgrids work in the surf command, we need to
    % transpose jvals before calling surf, or else the axes will be flipped
    jvals = jvals';
    % surface plot
    figure 2;
    surf(theta0, theta1, jvals)
    xlabel('\theta_0');
    ylabel('\theta_1');

    % contour plot
    figure 3;
    % plot jvals as 15 contours spaced logarithmically between 0.01 and 100
    contour(theta0, theta1, jvals, logspace(-2, 3, 20))
    xlabel('\theta_0');
    ylabel('\theta_1');
    hold on;
    plot(theta(1), theta(2), 'rx', 'MarkerSize', 2, 'LineWidth', 2);
end
