%% Multi-class Classification using Regularized Logistic Regression
%
%  This develops a model to predict a hand writtern digit depicted in gray
%  20x20 by training a subset of MNIST dataset stored in Matlab binary format.
%
%  Note: The following logistic regression was implemented as part of
%  the Andrew Ng's Coursera ML course. The data set and some of the code below
%  is from the Coursera ML course.
%
%  https://www.coursera.org/learn/machine-learning/
%
function multiClassClassification
    % initialization
    clear; close all;

    % add shared function path for sigmoid function
    addpath('../SharedFunctions');

    % setup the parameters
    input_layer_size = 400; % 20x20 input images of digits
    num_labels = 10;        % 10 labels, from 1 to 10 mapping 0 to 9

    % training data stored in arrays X, y
    load('ex3data1.mat');
    [m n] = size(X);

    % randomly select 100 data points to display
    rand_indices = randperm(m);
    sel = X(rand_indices(1:100), :);
    displayData(sel);

    fprintf('Program paused. Press enter to continue.\n\n');
    pause;

    % one vs all training
    lambda = 0.1;
    % add ones to the X data matrix
    X = [ones(m, 1) X];
    % initialize output theta
    all_theta = zeros(num_labels, n + 1);
    % set initial theta
    initial_theta = zeros(n + 1, 1);
    % set options
    options = optimset('GradObj', 'on', 'MaxIter', 50);
    % run fmincg to obtain the optimal theta
    for c = 1:num_labels
        [theta] = ...
            fmincg(@(t)(costFunction(t, X, (y == c), lambda)), ...
                initial_theta, options);
        all_theta(c, :) = theta';
    end

    fprintf('Program paused. Press enter to continue.\n\n');
    pause;

    % predict by applying all_theta to X and getting the indices of the maximum
    % probability which are the predicted value of the digit
    [prob indices] = max(sigmoid(X * all_theta'), [], 2);

    % compare indices which is the predicted values of the digit with labels
    fprintf('Training Set Accuracy: %f\n', mean(double(indices == y)) * 100);
end
