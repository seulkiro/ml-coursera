% Compute the cost using theta as the parameter for regularized logistic
% regression and the gradient of the cost w.r.t. to the parameters. Note
% Note that theta(1) is not regularized.
function [J, grad] = costFunction(theta, X, y, lambda)
    % number of training examples
    m = length(y);
    % compute h(x)
    h = sigmoid(theta' * X')';
    % compute cost without regularizing theta(1)
    tmp = theta(1);
    theta(1) = 0;
    J = (1/m)*sum((-y.*log(h))-(1-y).*log(1-h)) + (lambda/(2*m))*sum(theta.^2);
    theta(1) = tmp;
    % compute non-regularized gradient of cost wrt theta because theta(1) doesn't
    % need to be penalized
    nonRegularizedGrad = ((1/m)*((h-y)'*X));
    % compute regularized gradient of cost wrt theta
    grad = ((1/m)*((h-y)'*X)) + (lambda/m)*theta';
    % recover grad(1) with non-regularized gradient although it may not efficient
    % calculating two gradients with regularizaton and non-regularization
    % the actual implementation needs to improve this double calculation
    grad(1) = nonRegularizedGrad(1);
    % return transposed gradient vector
    grad = grad(:);
end
