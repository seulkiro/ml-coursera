%% Multi-class Classification using Neural Networks with Forward Propagation
%
%  This develops a model to predict a hand writtern digit depicted in gray
%  20x20 by training a subset of MNIST dataset stored in Matlab binary format.
%
%  Note: The following logistic regression was implemented as part of
%  the Andrew Ng's Coursera ML course. The data set and some of the code below
%  is from the Coursera ML course.
%
%  https://www.coursera.org/learn/machine-learning/
%
function nnMultiClassClassification
    % initialization
    clear; close all;

    % add shared function path for sigmoid function
    addpath('../SharedFunctions');

    % setup the parameters
    input_layer_size = 400; % 20x20 input images of digits
    hidden_layer_size = 25; % 25 hidden units
    num_labels = 10;        % 10 labels, from 1 to 10 mapping 0 to 9

    % load training data set
    fprintf('Loading and Visualizing Data ...\n')
    load('ex3data1.mat');
    [m n] = size(X);

    % randomly select 100 data points to display
    rand_indices = randperm(m);
    sel = X(rand_indices(1:100), :);
    displayData(sel);

    fprintf('Program paused. Press enter to continue.\n\n');
    pause;

    % load the pre-calculated weights into variables Theta1 and Theta2
    fprintf('Loading Saved Neural Network Parameters ...\n\n')
    load('ex3weights.mat');

    % predict the labels by using the neural networks and calculating the
    % activation values in each layers

    % add bias unit
    X = [ones(m,1) X];

    % compute activation values in hidden layer
    a2 = sigmoid(Theta1 * X')';
    % add bias unit
    a2 = [ones(m,1) a2];

    % compute activation values in output layer
    a3 = sigmoid(Theta2 * a2')';
    % get the indices of the maximum probability
    [prob indices] = max(a3, [], 2);

    % compare indices which is the predicted values of the digit with labels
    fprintf('Training Set Accuracy: %f\n', mean(double(indices == y)) * 100);
end
